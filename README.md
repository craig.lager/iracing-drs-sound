# 🎧 iracing-drs-sound

Configurable DRS and shift beeps for iRacing.

**DRS Sound**

Activates when DRS transitions from "can be activated next zone" to "can be activated now". This means that there is no DRS sound for FR 3.5 since it can always be deployed.

**Shift Sound**

Activates when RPM is over the configured RPM for the selected gear (defaults provided by iRacing) and throttle is over 75%

# ⬇ Download

https://gitlab.com/craig.lager/iracing-drs-sound/-/releases


# 📃 Configuration

## Tweaking shift points

`/config` holds all configurations for cars. You need to have loaded a car into iracing for a config to be created.

You can edit files directly, or edit the JSON in the drs-sounds window to tweak shifting points and other bits of configuration

## Changing the beep

Play with the values in `config/_globalconfig.json` and use the test buttons

## Debounce

Global Config has a debounce setting, which is the minimum amount of ticks (we run at 60 ticks per second) between shift beeps. This is useful for low gears for cars with ERS, you may need to tune it.


# 🙏 Supporting This Project

https://members.iracing.com/membersite/member/Recharge.do -> `Craig Lager` if you feel so inclined 😊
