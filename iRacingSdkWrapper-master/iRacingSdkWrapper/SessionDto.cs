﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;

//namespace iRacingSdkWrapper
//{
//    public partial class SessionDto
//    {
//        public WeekendInfo WeekendInfo { get; set; }

//        public SessionInfo2 SessionInfo { get; set; }

//        public CameraInfo CameraInfo { get; set; }

//        public RadioInfo RadioInfo { get; set; }

//        public DriverInfo DriverInfo { get; set; }

//        public SplitTimeInfo SplitTimeInfo { get; set; }

//        public object CarSetup { get; set; }
//    }

//    public partial class CameraInfo
//    {
//        public Group[] Groups { get; set; }
//    }

//    public partial class Group
//    {
//        public int GroupNum { get; set; }

//        public string GroupName { get; set; }

//        public Camera[] Cameras { get; set; }

//        public bool? IsScenic { get; set; }
//    }

//    public partial class Camera
//    {
//        public int CameraNum { get; set; }

//        public string CameraName { get; set; }
//    }

//    //public partial class CarSetup
//    //{
//    //    public int UpdateCount { get; set; }

//    //    public TiresAero TiresAero { get; set; }

//    //    public Chassis Chassis { get; set; }

//    //    public Drivetrain Drivetrain { get; set; }
//    //}

//    //public partial class Chassis
//    //{
//    //    public Front Front { get; set; }

//    //    public TFront LeftFront { get; set; }

//    //    public TRear LeftRear { get; set; }

//    //    public Rear Rear { get; set; }

//    //    public TFront RightFront { get; set; }

//    //    public TRear RightRear { get; set; }
//    //}

//    //public partial class Front
//    //{
//    //    public string ArbBellevilleStack { get; set; }

//    //    public string ArbPreload { get; set; }

//    //    public string PushrodLength { get; set; }

//    //    public string SpringPerchOffset { get; set; }

//    //    public string SpringRate { get; set; }

//    //    public string BumpStiffness { get; set; }

//    //    public string ReboundStiffness { get; set; }

//    //    public string FrontMasterCylinder { get; set; }

//    //    public string RearMasterCylinder { get; set; }

//    //    public string BrakePressureBias { get; set; }

//    //    public string DisplayPage { get; set; }
//    //}

//    //public partial class TFront
//    //{
//    //    public string CornerWeight { get; set; }

//    //    public string RideHeight { get; set; }

//    //    public string Camber { get; set; }

//    //    public string ToeIn { get; set; }
//    //}

//    //public partial class TRear
//    //{
//    //    public string CornerWeight { get; set; }

//    //    public string RideHeight { get; set; }

//    //    public string PushrodLength { get; set; }

//    //    public string SpringPerchOffset { get; set; }

//    //    public string SpringRate { get; set; }

//    //    public string BumpStiffness { get; set; }

//    //    public string ReboundStiffness { get; set; }

//    //    public string Camber { get; set; }

//    //    public string ToeIn { get; set; }
//    //}

//    //public partial class Rear
//    //{
//    //    public string FuelLevel { get; set; }

//    //    public string ArbDiameter { get; set; }

//    //    public string ArbArms { get; set; }

//    //    public string ArbPreload { get; set; }
//    //}

//    //public partial class Drivetrain
//    //{
//    //    public Gearbox Gearbox { get; set; }

//    //    public Differential Differential { get; set; }

//    //    public EngineInCarDials EngineInCarDials { get; set; }
//    //}

//    //public partial class Differential
//    //{
//    //    public int ClutchPlates { get; set; }

//    //    public string Preload { get; set; }

//    //    public string RampAngles { get; set; }
//    //}

//    //public partial class EngineInCarDials
//    //{
//    //    public string ThrottleShaping { get; set; }

//    //    public string OverrideLaunchRpm { get; set; }
//    //}

//    //public partial class Gearbox
//    //{
//    //    public string FirstGear { get; set; }

//    //    public string SecondGear { get; set; }

//    //    public string ThirdGear { get; set; }

//    //    public string FourthGear { get; set; }

//    //    public string FifthGear { get; set; }

//    //    public string SixthGear { get; set; }
//    //}

//    //public partial class TiresAero
//    //{
//    //    public LeftFront LeftFront { get; set; }

//    //    public LeftFront LeftRear { get; set; }

//    //    public LeftFront RightFront { get; set; }

//    //    public LeftFront RightRear { get; set; }

//    //    public Aero FrontAero { get; set; }

//    //    public Aero RearAero { get; set; }

//    //    public AeroCalculator AeroCalculator { get; set; }
//    //}

//    //public partial class AeroCalculator
//    //{
//    //    public string FrontRhAtSpeed { get; set; }

//    //    public string RearRhAtSpeed { get; set; }

//    //    public string FrontDownforce { get; set; }

//    //    public string DownforceToDrag { get; set; }
//    //}

//    //public partial class Aero
//    //{
//    //    public string WingFlapAngle { get; set; }
//    //}

//    //public partial class LeftFront
//    //{
//    //    public string ColdPressure { get; set; }

//    //    public string LastHotPressure { get; set; }

//    //    public string LastTempsOmi { get; set; }

//    //    public string TreadRemaining { get; set; }

//    //    public string LastTempsImo { get; set; }
//    //}

//    public partial class DriverInfo
//    {
//        public int DriverCarIdx { get; set; }

//        public int DriverUserId { get; set; }

//        public int PaceCarIdx { get; set; }

//        public double DriverHeadPosX { get; set; }

//        public int DriverHeadPosY { get; set; }

//        public double DriverHeadPosZ { get; set; }

//        public int DriverCarIdleRpm { get; set; }

//        public int DriverCarRedLine { get; set; }

//        public int DriverCarEngCylinderCount { get; set; }

//        public double DriverCarFuelKgPerLtr { get; set; }

//        public int DriverCarFuelMaxLtr { get; set; }

//        public int DriverCarMaxFuelPct { get; set; }

//        public int DriverCarGearNumForward { get; set; }

//        public int DriverCarGearNeutral { get; set; }

//        public int DriverCarGearReverse { get; set; }

//        public int DriverCarSlFirstRpm { get; set; }

//        public int DriverCarSlShiftRpm { get; set; }

//        public int DriverCarSlLastRpm { get; set; }

//        public int DriverCarSlBlinkRpm { get; set; }

//        public string DriverCarVersion { get; set; }

//        public double DriverPitTrkPct { get; set; }

//        public double DriverCarEstLapTime { get; set; }

//        public string DriverSetupName { get; set; }

//        public int DriverSetupIsModified { get; set; }

//        public string DriverSetupLoadTypeName { get; set; }

//        public int DriverSetupPassedTech { get; set; }

//        public int DriverIncidentCount { get; set; }

//        public Driver[] Drivers { get; set; }
//    }

//    public partial class Driver
//    {
//        public int CarIdx { get; set; }

//        public string UserName { get; set; }

//        public object AbbrevName { get; set; }

//        public object Initials { get; set; }

//        public int UserId { get; set; }

//        public int TeamId { get; set; }

//        public string TeamName { get; set; }

//        public int CarNumber { get; set; }

//        public int CarNumberRaw { get; set; }

//        public string CarPath { get; set; }

//        public int CarClassId { get; set; }

//        public int CarId { get; set; }

//        public int CarIsPaceCar { get; set; }

//        public int CarIsAi { get; set; }

//        public string CarScreenName { get; set; }

//        public string CarScreenNameShort { get; set; }

//        public object CarClassShortName { get; set; }

//        public int CarClassRelSpeed { get; set; }

//        public int CarClassLicenseLevel { get; set; }

//        public string CarClassMaxFuelPct { get; set; }

//        public string CarClassWeightPenalty { get; set; }

//        public string CarClassPowerAdjust { get; set; }

//        public string CarClassDryTireSetLimit { get; set; }

//        public int CarClassColor { get; set; }

//        public double CarClassEstLapTime { get; set; }

//        public int IRating { get; set; }

//        public int LicLevel { get; set; }

//        public int LicSubLevel { get; set; }

//        public string LicString { get; set; }

//        public int LicColor { get; set; }

//        public int IsSpectator { get; set; }

//        public string CarDesignStr { get; set; }

//        public string HelmetDesignStr { get; set; }

//        public string SuitDesignStr { get; set; }

//        public string CarNumberDesignStr { get; set; }

//        public int CarSponsor1 { get; set; }

//        public int CarSponsor2 { get; set; }

//        public int CurDriverIncidentCount { get; set; }

//        public int TeamIncidentCount { get; set; }
//    }

//    public partial class RadioInfo
//    {
//        public int SelectedRadioNum { get; set; }

//        public Radio[] Radios { get; set; }
//    }

//    public partial class Radio
//    {
//        public int RadioNum { get; set; }

//        public int HopCount { get; set; }

//        public int NumFrequencies { get; set; }

//        public int TunedToFrequencyNum { get; set; }

//        public int ScanningIsOn { get; set; }

//        public Frequency[] Frequencies { get; set; }
//    }

//    public partial class Frequency
//    {
//        public int FrequencyNum { get; set; }

//        public string FrequencyName { get; set; }

//        public int Priority { get; set; }

//        public int CarIdx { get; set; }

//        public int EntryIdx { get; set; }

//        public int ClubId { get; set; }

//        public int CanScan { get; set; }

//        public int CanSquawk { get; set; }

//        public int Muted { get; set; }

//        public int IsMutable { get; set; }

//        public int IsDeletable { get; set; }
//    }

//    public partial class SessionInfo2
//    {
//        public Session[] Sessions { get; set; }
//    }

//    public partial class Session
//    {
//        public int SessionNum { get; set; }

//        public string SessionLaps { get; set; }

//        public string SessionTime { get; set; }

//        public int SessionNumLapsToAvg { get; set; }

//        public string SessionType { get; set; }

//        public string SessionTrackRubberState { get; set; }

//        public string SessionName { get; set; }

//        public object SessionSubType { get; set; }

//        public int SessionSkipped { get; set; }

//        public int SessionRunGroupsUsed { get; set; }

//        public object ResultsPositions { get; set; }

//        public ResultsFastestLap[] ResultsFastestLap { get; set; }

//        public int ResultsAverageLapTime { get; set; }

//        public int ResultsNumCautionFlags { get; set; }

//        public int ResultsNumCautionLaps { get; set; }

//        public int ResultsNumLeadChanges { get; set; }

//        public int ResultsLapsComplete { get; set; }

//        public int ResultsOfficial { get; set; }
//    }

//    public partial class ResultsFastestLap
//    {
//        public int CarIdx { get; set; }

//        public int FastestLap { get; set; }

//        public int FastestTime { get; set; }
//    }

//    public partial class SplitTimeInfo
//    {
//        public Sector[] Sectors { get; set; }
//    }

//    public partial class Sector
//    {
//        public int SectorNum { get; set; }

//        public double SectorStartPct { get; set; }
//    }

//    public partial class WeekendInfo
//    {
//        public string TrackName { get; set; }

//        public string TrackID { get; set; }

//        public string TrackLength { get; set; }

//        public string TrackDisplayName { get; set; }

//        public string TrackDisplayShortName { get; set; }

//        public object TrackConfigName { get; set; }

//        public string TrackCity { get; set; }

//        public string TrackCountry { get; set; }

//        public string TrackAltitude { get; set; }

//        public string TrackLatitude { get; set; }

//        public string TrackLongitude { get; set; }

//        public string Trackintitude { get; set; }

//        public string TrackNorthOffset { get; set; }

//        public int TrackNumTurns { get; set; }

//        public string TrackPitSpeedLimit { get; set; }

//        public string TrackType { get; set; }

//        public string TrackDirection { get; set; }

//        public string TrackWeatherType { get; set; }

//        public string TrackSkies { get; set; }

//        public string TrackSurfaceTemp { get; set; }

//        public string TrackAirTemp { get; set; }

//        public string TrackAirPressure { get; set; }

//        public string TrackWindVel { get; set; }

//        public string TrackWindDir { get; set; }

//        public string TrackRelativeHumidity { get; set; }

//        public string TrackFogLevel { get; set; }

//        public int TrackCleanup { get; set; }

//        public int TrackDynamicTrack { get; set; }

//        public string TrackVersion { get; set; }

//        public int SeriesId { get; set; }

//        public int SeasonId { get; set; }

//        public int SessionId { get; set; }

//        public int SubSessionId { get; set; }

//        public int LeagueId { get; set; }

//        public int Official { get; set; }

//        public int RaceWeek { get; set; }

//        public string EventType { get; set; }

//        public string Category { get; set; }

//        public string SimMode { get; set; }

//        public int TeamRacing { get; set; }

//        public int MinDrivers { get; set; }

//        public int MaxDrivers { get; set; }

//        public string DcRuleSet { get; set; }

//        public int QualifierMustStartRace { get; set; }

//        public int NumCarClasses { get; set; }

//        public int NumCarTypes { get; set; }

//        public int HeatRacing { get; set; }

//        public string BuildType { get; set; }

//        public string BuildTarget { get; set; }

//        public string BuildVersion { get; set; }

//        public WeekendOptions WeekendOptions { get; set; }

//        public TelemetryOptions TelemetryOptions { get; set; }
//    }

//    public partial class TelemetryOptions
//    {
//        public string TelemetryDiskFile { get; set; }
//    }

//    public partial class WeekendOptions
//    {
//        public int NumStarters { get; set; }

//        public string StartingGrid { get; set; }

//        public string QualifyScoring { get; set; }

//        public string CourseCautions { get; set; }

//        public int StandingStart { get; set; }

//        public int ShortParadeLap { get; set; }

//        public string Restarts { get; set; }

//        public string WeatherType { get; set; }

//        public string Skies { get; set; }

//        public string WindDirection { get; set; }

//        public string WindSpeed { get; set; }

//        public string WeatherTemp { get; set; }

//        public string RelativeHumidity { get; set; }

//        public string FogLevel { get; set; }

//        public string TimeOfDay { get; set; }

//        public DateTimeOffset Date { get; set; }

//        public int EarthRotationSpeedupFactor { get; set; }

//        public int Unofficial { get; set; }

//        public string CommercialMode { get; set; }

//        public string NightMode { get; set; }

//        public int IsFixedSetup { get; set; }

//        public string StrictLapsChecking { get; set; }

//        public int HasOpenRegistration { get; set; }

//        public int HardcoreLevel { get; set; }

//        public int NumJokerLaps { get; set; }

//        public string IncidentLimit { get; set; }

//        public string FastRepairsLimit { get; set; }

//        public int GreenWhiteCheckeredLimit { get; set; }
//    }
//}