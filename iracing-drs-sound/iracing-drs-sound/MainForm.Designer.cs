﻿
namespace iracing_drs_sound
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (this.IRacingManager != null)
            {
                this.IRacingManager.Dispose();
            }

            if (disposing && (components != null))
            {

                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Lbl_ConnectionStatus = new System.Windows.Forms.Label();
            this.Txt_Config = new System.Windows.Forms.TextBox();
            this.Btn_SaveConfig = new System.Windows.Forms.Button();
            this.Btn_Reset = new System.Windows.Forms.Button();
            this.Btn_TestDrsSound = new System.Windows.Forms.Button();
            this.Btn_TestShiftSound = new System.Windows.Forms.Button();
            this.Txt_Info = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // Lbl_ConnectionStatus
            // 
            this.Lbl_ConnectionStatus.AutoSize = true;
            this.Lbl_ConnectionStatus.Location = new System.Drawing.Point(12, 9);
            this.Lbl_ConnectionStatus.Name = "Lbl_ConnectionStatus";
            this.Lbl_ConnectionStatus.Size = new System.Drawing.Size(79, 15);
            this.Lbl_ConnectionStatus.TabIndex = 0;
            this.Lbl_ConnectionStatus.Text = "Disconnected";
            // 
            // Txt_Config
            // 
            this.Txt_Config.Location = new System.Drawing.Point(12, 28);
            this.Txt_Config.Multiline = true;
            this.Txt_Config.Name = "Txt_Config";
            this.Txt_Config.Size = new System.Drawing.Size(477, 347);
            this.Txt_Config.TabIndex = 1;
            // 
            // Btn_SaveConfig
            // 
            this.Btn_SaveConfig.Location = new System.Drawing.Point(12, 382);
            this.Btn_SaveConfig.Name = "Btn_SaveConfig";
            this.Btn_SaveConfig.Size = new System.Drawing.Size(75, 23);
            this.Btn_SaveConfig.TabIndex = 2;
            this.Btn_SaveConfig.Text = "Save";
            this.Btn_SaveConfig.UseVisualStyleBackColor = true;
            this.Btn_SaveConfig.Click += new System.EventHandler(this.Btn_SaveConfig_Click);
            // 
            // Btn_Reset
            // 
            this.Btn_Reset.Location = new System.Drawing.Point(93, 382);
            this.Btn_Reset.Name = "Btn_Reset";
            this.Btn_Reset.Size = new System.Drawing.Size(75, 23);
            this.Btn_Reset.TabIndex = 5;
            this.Btn_Reset.Text = "Reset";
            this.Btn_Reset.UseVisualStyleBackColor = true;
            this.Btn_Reset.Click += new System.EventHandler(this.Btn_Reset_Click);
            // 
            // Btn_TestDrsSound
            // 
            this.Btn_TestDrsSound.Location = new System.Drawing.Point(378, 382);
            this.Btn_TestDrsSound.Name = "Btn_TestDrsSound";
            this.Btn_TestDrsSound.Size = new System.Drawing.Size(110, 23);
            this.Btn_TestDrsSound.TabIndex = 7;
            this.Btn_TestDrsSound.Text = "Test DRS Sound";
            this.Btn_TestDrsSound.UseVisualStyleBackColor = true;
            this.Btn_TestDrsSound.Click += new System.EventHandler(this.Btn_TestDrsSound_Click);
            // 
            // Btn_TestShiftSound
            // 
            this.Btn_TestShiftSound.Location = new System.Drawing.Point(262, 382);
            this.Btn_TestShiftSound.Name = "Btn_TestShiftSound";
            this.Btn_TestShiftSound.Size = new System.Drawing.Size(110, 23);
            this.Btn_TestShiftSound.TabIndex = 8;
            this.Btn_TestShiftSound.Text = "Test Shift Sound";
            this.Btn_TestShiftSound.UseVisualStyleBackColor = true;
            this.Btn_TestShiftSound.Click += new System.EventHandler(this.Btn_TestShiftSound_Click);
            // 
            // Txt_Info
            // 
            this.Txt_Info.Location = new System.Drawing.Point(496, 28);
            this.Txt_Info.Multiline = true;
            this.Txt_Info.Name = "Txt_Info";
            this.Txt_Info.Size = new System.Drawing.Size(268, 347);
            this.Txt_Info.TabIndex = 9;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(779, 418);
            this.Controls.Add(this.Txt_Info);
            this.Controls.Add(this.Btn_TestShiftSound);
            this.Controls.Add(this.Btn_TestDrsSound);
            this.Controls.Add(this.Btn_Reset);
            this.Controls.Add(this.Btn_SaveConfig);
            this.Controls.Add(this.Txt_Config);
            this.Controls.Add(this.Lbl_ConnectionStatus);
            this.Name = "MainForm";
            this.Text = "MainForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainForm_FormClosed);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label Lbl_ConnectionStatus;
        private System.Windows.Forms.TextBox Txt_Config;
        private System.Windows.Forms.Button Btn_SaveConfig;
        private System.Windows.Forms.Button Btn_Reset;
        private System.Windows.Forms.Button Btn_TestDrsSound;
        private System.Windows.Forms.Button Btn_TestShiftSound;
        private System.Windows.Forms.TextBox Txt_Info;
    }
}