﻿using iracing_drs_sound.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace iracing_drs_sound
{
    public partial class MainForm : Form
    {

        public MainForm()
        {
            InitializeComponent();
            this.IRacingManager = new Models.IRacingManager();
            this.IRacingManager.OnChange += IRacingManager_OnChange;
            this.IRacingManager.OnEvent += IRacingManager_OnEvent;
        }

        private void IRacingManager_OnEvent(object sender, IRacingManager.ManagerEvent e)
        {
            this.Txt_Info.AppendText(e.EventMessage + Environment.NewLine);
            if(this.Txt_Info.Text.Length > 1000)
            {
                this.Txt_Info.Text = this.Txt_Info.Text.Substring(500);
            }
        }

        private void IRacingManager_OnChange(object sender, EventArgs e)
        {
            this.Txt_Config.Text = this.IRacingManager.ConfigString;
            this.Lbl_ConnectionStatus.Text = this.IRacingManager.ConnectionStatus;
        }

        decimal Median(int[] xs)
        {
            Array.Sort(xs);
            return xs[xs.Length / 2];
        }

        public IRacingManager IRacingManager { get; }

        private void Btn_SaveConfig_Click(object sender, EventArgs e)
        {
            this.IRacingManager.CarConfig = Models.CarConfig.Save(Txt_Config.Text);
        }

        private void MainForm_FormClosed(object sender, FormClosedEventArgs e)
        {  
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
        }

        private void Btn_Reset_Click(object sender, EventArgs e)
        {
            var confirm = MessageBox.Show("This will reset config for this car to default, are you sure?", "Confirm", MessageBoxButtons.OKCancel);

            if(confirm != DialogResult.OK)
            {
                return;
            }

            this.IRacingManager.ResetCarConfig();
        }

        private void Btn_TestDrsSound_Click(object sender, EventArgs e)
        {
            this.IRacingManager.GlobalConfig = GlobalConfig.Load();
            new BeepService(this.IRacingManager.GlobalConfig).DrsBeep(true);
        }

        private void Btn_TestShiftSound_Click(object sender, EventArgs e)
        {
            this.IRacingManager.GlobalConfig = GlobalConfig.Load();
            new BeepService(this.IRacingManager.GlobalConfig).ShiftBeep(true);
        }
    }
}
