﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iracing_drs_sound.Models
{
    public class BeepService
    {
        public GlobalConfig GlobalConfig { get; set; }

        public BeepService(GlobalConfig config)
        {
            this.GlobalConfig = config;
        }

        internal void DrsBeep(bool test = false)
        {
            if(test)
            {
                this.GlobalConfig = Models.GlobalConfig.Load();
            }
            Beep(GlobalConfig.DrsBeepFrequency, GlobalConfig.DrsBeepDuration);
        }

        internal void ShiftBeep(bool test = false)
        {
            if (test)
            {
                this.GlobalConfig = Models.GlobalConfig.Load();
            }
            Beep(GlobalConfig.ShiftBeepFrequency, GlobalConfig.ShiftBeepDuration);
        }

        private void Beep(int frequency, int duration)
        {
            Task.Run(() =>
            {
                Console.Beep(frequency, duration);
            });
        }
    }
}
