﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iracing_drs_sound.Models
{
    public class GlobalConfig
    {
        public int DrsBeepFrequency
        {
            get; set;
        }

        public int DrsBeepDuration
        {
            get;set;
        }

        public int ShiftBeepFrequency
        {
            get; set;
        }

        public int ShiftBeepDuration
        {
            get; set;
        }

        public int ShiftDebounce
        {
            get; set;
        }

        internal static GlobalConfig Load()
        {
            var file = @"configs\_globalconfig.json";

            if (!System.IO.File.Exists(file))
            {
                var config = new GlobalConfig()
                {
                    DrsBeepDuration = 500,
                    DrsBeepFrequency = 1000,
                    ShiftBeepDuration = 200,
                    ShiftBeepFrequency = 1500,
                    ShiftDebounce = 50
                };

                using(var f = System.IO.File.CreateText(file))
                {
                    f.Write(Newtonsoft.Json.JsonConvert.SerializeObject(config));
                }

                return config;
            }

            var fileContents = System.IO.File.ReadAllText(file);
            return Newtonsoft.Json.JsonConvert.DeserializeObject<Models.GlobalConfig>(fileContents);
        }
    }
}
