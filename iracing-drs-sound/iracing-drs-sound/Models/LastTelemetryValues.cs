﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iracing_drs_sound.Models
{
    internal class LastTelemetryValues
    {
        public int DrsStatus { get; internal set; }
        public int Gear { get; internal set; }
    }
}
