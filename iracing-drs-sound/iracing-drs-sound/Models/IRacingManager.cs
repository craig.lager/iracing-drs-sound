﻿using iRacingSdkWrapper;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iracing_drs_sound.Models
{
    public class IRacingManager : IDisposable
    {
        private LastTelemetryValues _lastTelemetryValues;
        private bool disposedValue;

        private bool ShiftSoundedForCurrentGear { get; set; }

        private int _shiftDebounceCounter;

        public SdkWrapper.SessionInfoUpdatedEventArgs SessionInfo { get; private set; }

        internal Models.CarConfig _carConfig;

        internal Models.CarConfig CarConfig { get { return _carConfig; } set { _carConfig = value; ConfigString = Newtonsoft.Json.JsonConvert.SerializeObject(value, Newtonsoft.Json.Formatting.Indented); } }

        private SdkWrapper Wrapper { get; }

        internal string ConfigString { get; set; }

        internal string ConnectionStatus { get; set; }
        public GlobalConfig GlobalConfig { get; set; }
        internal BeepService BeepService { get; set; }

        internal int LastNonZeroGear { get; set; }

        public event EventHandler OnChange;
        public event EventHandler<ManagerEvent> OnEvent;

        public class ManagerEvent : EventArgs
        {
            public string EventMessage { get; set; }
        }

        public void OnChangeEvent()
        {
            OnChange?.Invoke(this, null);
        }

        public void OnGenericEvent(string message)
        {
            OnEvent?.Invoke(this, new ManagerEvent() { EventMessage = message });
        }

        public IRacingManager()
        {
            this.GlobalConfig = GlobalConfig.Load();
            this.BeepService = new BeepService(this.GlobalConfig);
            this.LastNonZeroGear = 1;

            // Create instance
            this.Wrapper = new SdkWrapper();
            // Listen to events
            this.Wrapper.TelemetryUpdated += OnTelemetryUpdated;
            this.Wrapper.SessionInfoUpdated += Wrapper_SessionInfoUpdated;
            this.Wrapper.Connected += Wrapper_Connected;
            this.Wrapper.Disconnected += Wrapper_Disconnected;

            // Start it!
            this.Wrapper.Start();
        }


        private void Wrapper_SessionInfoUpdated(object sender, SdkWrapper.SessionInfoUpdatedEventArgs e)
        {
            if (this.CarConfig != null)
            {
                return;
            }
            this.SessionInfo = e;
            this.CarConfig = Models.CarConfig.Get(e);
            this.LastNonZeroGear = 1;
            OnChangeEvent();
        }

        decimal Median(int[] xs)
        {
            Array.Sort(xs);
            return xs[xs.Length / 2];
        }

        internal void ResetCarConfig()
        {
            this.CarConfig.Delete();
            this.CarConfig = Models.CarConfig.Get(this.SessionInfo);
            OnChangeEvent();
        }

        private void Wrapper_Disconnected(object sender, EventArgs e)
        {
            this.ConnectionStatus = "Disconnected";
            this.CarConfig = null;
            this.ConfigString = "";
            OnChangeEvent();
        }

        private void Wrapper_Connected(object sender, EventArgs e)
        {
            this.ConnectionStatus = "Connected";
            OnChangeEvent();
        }

        private void OnTelemetryUpdated(object sender, SdkWrapper.TelemetryUpdatedEventArgs e)
        {
            if (e.TelemetryInfo.IsReplayPlaying.Value || this.CarConfig == null || !this.CarConfig.ShiftSoundRPM.Any())
            {
                return;
            }

            if (this._lastTelemetryValues != null)
            {
                var currentGear = e.TelemetryInfo.Gear.Value;

                if (this.CarConfig.DrsSoundEnabled)
                {
                    // beep when we transition from "can be activated next zone" to "can be activated now"
                    if (this._lastTelemetryValues.DrsStatus == 1 && e.TelemetryInfo.DrsStatus.Value == 2)
                    {
                        OnGenericEvent("DRS Ready");
                        this.BeepService.DrsBeep();
                    }
                }

                if(this.CarConfig.ShiftSoundEnabled && currentGear > 0)
                {
                    if (_shiftDebounceCounter > 0)
                    {
                        _shiftDebounceCounter--;
                    }

                    if (e.TelemetryInfo.Throttle.Value < .75)
                    {
                        this.ShiftSoundedForCurrentGear = false;
                    }

                    if (currentGear <= this.CarConfig.ShiftSoundRPM.Count() && currentGear != LastNonZeroGear)
                    {
                        this.ShiftSoundedForCurrentGear = false;
                        this._shiftDebounceCounter = this.GlobalConfig.ShiftDebounce;
                    }

                    if (currentGear <= this.CarConfig.ShiftSoundRPM.Count())
                    {
                        var targetRpm = this.CarConfig.ShiftSoundRPM[currentGear - 1];


                        // beep when RPM is higher than current gear target
                        if (currentGear > 0
                            && this._shiftDebounceCounter <= 0
                            && currentGear <= this.CarConfig.ShiftSoundRPM.Count()
                            && !this.ShiftSoundedForCurrentGear
                            && e.TelemetryInfo.RPM.Value >= targetRpm
                            && e.TelemetryInfo.Throttle.Value > 0.75)
                        {
                            this.ShiftSoundedForCurrentGear = true;
                            this._shiftDebounceCounter = this.GlobalConfig.ShiftDebounce;
                            
                            OnGenericEvent("Shift ready: g" + currentGear + ":" + e.TelemetryInfo.RPM);
                            Debug.WriteLine("Shift:" + currentGear + ":" + e.TelemetryInfo.RPM);
                            this.BeepService.ShiftBeep();
                        }
                    }

                    LastNonZeroGear = currentGear;
                }
            }

            this._lastTelemetryValues = new LastTelemetryValues()
            {
                DrsStatus = e.TelemetryInfo.DrsStatus.Value,
                Gear = e.TelemetryInfo.Gear.Value
            };
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    Wrapper.Stop();
                }

                // TODO: free unmanaged resources (unmanaged objects) and override finalizer
                // TODO: set large fields to null
                disposedValue = true;
            }
        }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
}
