﻿using iRacingSdkWrapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iracing_drs_sound.Models
{
    public class CarConfig
    {
        public CarConfig() {
            ShiftSoundEnabled = true;
            DrsSoundEnabled = true;
            ShiftSoundRPM = new List<int>();
        }
        public CarConfig(string carPath) : this()
        {
            this.CarPath = carPath;

        }

        public string CarPath { get; set; }
        public bool ShiftSoundEnabled { get; set; }
        public List<int> ShiftSoundRPM { get; set; }
        

        public bool DrsSoundEnabled { get; set; }

        private string GetConfigPath()
        {
            return GetConfigPath(this.CarPath);
        }

        private static string GetConfigPath(string carPath)
        {
            return System.IO.Path.GetFullPath("configs\\" + carPath + ".json");
        }

        private static bool ConfigExists(string carPath)
        {
            return System.IO.File.Exists(GetConfigPath(carPath));
        }

        private static CarConfig Load(string carPath)
        {
            var configFile = GetConfigPath(carPath);
            var file = System.IO.File.ReadAllText(configFile);
            return Newtonsoft.Json.JsonConvert.DeserializeObject<Models.CarConfig>(file);
        }

        public static CarConfig Get(SdkWrapper.SessionInfoUpdatedEventArgs e)
        {
            var carPath = GetCarPath(e.SessionInfo);
            if (Models.CarConfig.ConfigExists(carPath))
            {
                return Models.CarConfig.Load(carPath);
            }
            else
            {
                return Models.CarConfig.Create(e.SessionInfo);
            }
        }

        public void Save()
        {
            if(ConfigExists(this.CarPath))
            {
                System.IO.File.Delete(GetConfigPath());
            }

            using (var file = System.IO.File.CreateText(GetConfigPath()))
            {
                file.Write(Newtonsoft.Json.JsonConvert.SerializeObject(this));
                file.Flush();
            }
        }

        internal static CarConfig Create(SessionInfo sessionInfo)
        {
            var carPath = GetCarPath(sessionInfo);
            var config = new Models.CarConfig(carPath);
            var gears = int.Parse(sessionInfo["DriverInfo"]["DriverCarGearNumForward"].Value);
            var baseDriverCarSLLastRPM = float.Parse(sessionInfo["DriverInfo"]["DriverCarSLShiftRPM"].Value);
            for (var i = 0; i < gears-1; i++)
            {
                config.ShiftSoundRPM.Add((int)baseDriverCarSLLastRPM);
            }

            config.Save();

            return config;

        }

        internal void Delete()
        {
            if (ConfigExists(this.CarPath))
            {
                System.IO.File.Delete(GetConfigPath());
            }
        }

        internal static string GetCarPath(SessionInfo sessionInfo)
        {
            var driverIdx = int.Parse(sessionInfo["DriverInfo"]["DriverCarIdx"].Value);
            var raw = sessionInfo.RawYaml.Split("\n").ToList();
            var drivers = raw.IndexOf(" Drivers:");
            var driverStart = raw.IndexOf(" - CarIdx: " + driverIdx, drivers);
            for (var i = driverStart; i < driverStart + 20; i++)
            {
                var line = raw.ElementAt(i);
                if (line.Contains("CarPath"))
                {
                    return line.Replace("CarPath:", "").Trim();
                }
            }

            return null;
        }

        public static CarConfig Save(string strConfig)
        {
            try
            {
                var config = Newtonsoft.Json.JsonConvert.DeserializeObject<Models.CarConfig>(strConfig);
                config.Save();
                return config;
            }
            catch (Exception ex)
            {
               //MessageBox.Show(ex.Message);
                return null;
            }
        }
    }



}
